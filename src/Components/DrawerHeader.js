import React from 'react';
import { Text } from "react-native";
import { Header, Body, Left, Right, Icon } from "native-base";

const DrawerHeader = ({navigation, MenuName, right}) => {
    return (
        <Header>
            <Left style={{paddingLeft:10}}>
                <Icon
                    name="ios-menu"
                    onPress={() => navigation.openDrawer('DrawerOpen')} 
                    style={{color: 'white'}}
                />
            </Left>
            <Body>
                <Text style={{fontSize: 18, fontWeight: 'bold', color: 'white'}}>{MenuName}</Text>
            </Body>
            {right == 1 ?
                <Right><Icon
                    name="add"
                    onPress={() => navigation.openDrawer('DrawerOpen')} 
                    style={{color: 'white'}}
                /></Right>
            : <Right /> }
        </Header>
    );
};

export default DrawerHeader;