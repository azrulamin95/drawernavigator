import React from "react";
import { View, Dimensions } from "react-native";
import { Text, Card, CardItem } from "native-base";
import FontAwesome from "react-native-vector-icons/FontAwesome";

const WIDTH = Dimensions.get('window').width - 20;

const CartListItem = ({icon, detail}) => {
    return (
        <Card style={{width: WIDTH/3, height: 80, backgroundColor:'white'}}>
            <CardItem header button style={{flex: 1, justifyContent: 'center', backgroundColor:'white'}}>
                <FontAwesome name={icon} size={26}/>
            </CardItem>
            <CardItem footer button style={{backgroundColor:'white'}}>
                <View style={{flex: 1, flexDirection:'row', justifyContent: 'center'}}>
                    {detail.indexOf('.') == -1 ? 
                        <Text style={{fontSize:14}}>{detail} Item</Text> : 
                        <Text style={{fontSize:14}}>RM {detail}</Text>
                    }
                </View>
            </CardItem>
        </Card>
    );
};

export default CartListItem;