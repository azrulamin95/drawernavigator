import React from 'react';
import { Text } from "react-native";
import { Header, Body, Left, Right, Icon } from "native-base";

const StackNavigatorHeader = ({navigation, MenuName}) => {
    const goback = (navigation) => {
        navigation.goBack(null);
    };
    return (
        <Header>
            <Left style={{paddingLeft:10}}>
                <Icon
                    name="ios-arrow-back"
                    onPress={() => goback(navigation)} 
                    style={{color: 'white'}}
                />
            </Left>
            <Body>
                <Text style={{fontSize: 18, fontWeight: 'bold', color: 'white'}}>{MenuName}</Text>
            </Body>
            <Right />
        </Header>
    );
};

export default StackNavigatorHeader;