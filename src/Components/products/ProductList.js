import React from 'react';
import { View, Text, StyleSheet, Image, TouchableHighlight } from 'react-native';

const CustomRow = ({ navigation, title, price, img, list }) => (
    <TouchableHighlight>
        <View style={styles.container}>
            <Image source={img} style={styles.photo} />
            <View style={styles.container_text}>
                <Text style={styles.title}>
                    {title}
                </Text>
                <Text style={styles.description}>
                {list == 'product' ? 'RM' : null} {price} {list == 'stock' ? 'Unit' : null}
                </Text>
            </View>
        </View>
    </TouchableHighlight>
);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'row',
        padding: 10,
        marginTop: 5,
        borderRadius: 5,
        backgroundColor: '#FFF',
        elevation: 2,
    },
    title: {
        fontSize: 16,
        color: '#000',
        alignSelf:'center',
        fontStyle: 'italic',
    },
    container_text: {
        flex: 1,
        flexDirection: 'row',
        marginLeft: 12,
        justifyContent: 'space-between',
    },
    description: {
        fontSize: 16,
        fontStyle: 'italic',
        alignSelf:'center'
    },
    photo: {
        height: 50,
        width: 60,
    },
});

export default CustomRow;