import React, {useState} from "react";
import { Input } from "native-base";
import { StyleSheet, View } from "react-native";
import AntDesign from "react-native-vector-icons/AntDesign";
import FontAwesome from "react-native-vector-icons/FontAwesome";

const ProductSearchTabs = ({navigation}) => {
    const [showSearch, setShowSearch] = useState(false);
    return (
        <View style={{backgroundColor:'rgb(245, 245, 245)', height: 50}}>
            {showSearch == false ?
            <View style={styles.head_container}>
                <FontAwesome name="search" 
                    size={22}
                    onPress={() => setShowSearch(true)} 
                />
                <Input placeholder="Search Products" style={{ height: 22, marginLeft: 15}}/>
                <AntDesign name="plus" size={22}/>
            </View>: <View style={styles.head_container}>
                <AntDesign name="close" 
                    size={22} 
                    onPress={() => setShowSearch(false)}
                />
                <Input placeholder="Search Products" style={{ height: 22, marginLeft: 15}}/>
            </View>}
        </View>
    )
};

const styles = StyleSheet.create({
    head_container:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop: 15,
        marginHorizontal : 10,
        height: 30,
    }
});

export default ProductSearchTabs;