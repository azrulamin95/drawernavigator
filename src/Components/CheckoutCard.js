import React from "react";
import { View, Dimensions } from "react-native";
import { Text, Card, CardItem } from "native-base";
import Ionicons from "react-native-vector-icons/Ionicons";

const WIDTH = Dimensions.get('window').width - 20;

const CheckoutCard = ({product,category,price}) => {
    return (
        <Card style={{width: WIDTH/3, height: 100, backgroundColor:'#F1EFEF'}}>
            {product == '+' ? 
                <CardItem header button style={{flex: 1, justifyContent: 'center', backgroundColor:'#F1EFEF'}}>
                    <Ionicons name="md-add" size={26}/>
                </CardItem> :
                <CardItem header button style={{justifyContent: 'center', backgroundColor:'#F1EFEF'}}>
                    <Text>{product}</Text>
                </CardItem>
            }
            {product != '+' && product != '' ? 
                <CardItem footer button style={{backgroundColor:'#F1EFEF'}}>
                    <View style={{flex: 1, flexDirection:'row', justifyContent: 'space-between'}}>
                        <Text style={{fontSize:10}}>
                            <Ionicons name="md-pricetags" size={12}/>
                            {category}
                        </Text>
                        <Text style={{fontSize:10}}>RM{price}</Text>
                    </View>
                </CardItem> : null
            }
        </Card>
    );
};

export default CheckoutCard;