import React, { useState } from "react";
import { Container, Content, Picker, Form, Button, Input } from "native-base";
import { View, Text, StyleSheet } from "react-native";
import Feather from "react-native-vector-icons/Feather";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import AntDesign from "react-native-vector-icons/AntDesign";

import DrawerHeader from "../Components/DrawerHeader";
import CheckoutCard from "../Components/CheckoutCard";

const CheckOutScreen = ({navigation}) => {
    const [showSearch, setShowSearch] = useState(false);
    return (
        <Container>
            <DrawerHeader navigation={navigation} MenuName="Checkout"/>
            <Content>
                {showSearch == false ?
                    <View style={styles.head_container}>
                        <View style={styles.picker}>
                            <Form>
                                <Picker
                                    note
                                    mode="dropdown"
                                    style={{ width: 150, height: 22 }}
                                >
                                    <Picker.Item label="Categories" value="key0" />
                                    <Picker.Item label="Stationery" value="key1" />
                                    <Picker.Item label="Toy" value="key2" />
                                    <Picker.Item label="Perfume" value="key3" />
                                    <Picker.Item label="Fashion" value="key4" />
                                </Picker>
                            </Form>
                        </View>
                        <View style={{flexDirection:'row'}}>
                            <FontAwesome name="search" 
                                size={22} 
                                style={styles.marginRight}
                                onPress={() => setShowSearch(true)}
                            />
                            <FontAwesome 
                                name="barcode" 
                                size={22} 
                                style={styles.marginRight} 
                                onPress={() => navigation.navigate('ScanProductScreen')}
                            />
                            <AntDesign name="bars" size={22}/>
                        </View>
                    </View> : <View style={styles.head_container}>
                        <AntDesign name="close" 
                            size={22} 
                            onPress={() => setShowSearch(false)}
                        />
                        <Input placeholder="Search Products" style={{ height: 22}}/>
                </View> }

                <View style={styles.body_container}>
                    <CheckoutCard product="MATH" category="Book" price="15.00"/>
                    <CheckoutCard product="ENGLISH" category="Book" price="12.00"/>
                    <CheckoutCard product="CHEM" category="Book" price="45.00"/>
                    <CheckoutCard product="PHY" category="Book" price="40.00"/>
                    <CheckoutCard product="+" category="" price=""/>
                    <CheckoutCard product="" category="" price=""/>
                    <CheckoutCard product="" category="" price=""/>
                    <CheckoutCard product="" category="" price=""/>
                    <CheckoutCard product="" category="" price=""/>
                    <CheckoutCard product="" category="" price=""/>
                    <CheckoutCard product="" category="" price=""/>
                    <CheckoutCard product="" category="" price=""/>
                </View>
            </Content>
                <View style={styles.footer_container}>
                    <Button block light onPress={() => navigation.navigate('Cart')}>
                        <Text>2 item = RM39.00</Text>
                    </Button>
                </View>
        </Container>
    )
};

const styles = StyleSheet.create({
    head_container:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop: 15,
        marginHorizontal : 5,
        height: 30
    },
    body_container:{
        flexDirection:'row',
        marginTop: 20,
        marginHorizontal : 3,
        flexWrap: 'wrap',
        alignItems: 'flex-start'
    },
    footer_container:{
        marginHorizontal: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#bdc3c7', 
        overflow: 'hidden',
        marginBottom: 30
    },
    picker:{
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#bdc3c7', 
        overflow: 'hidden'
    },
    marginRight:{
        marginRight: 10,
    }
});

CheckOutScreen.navigationOptions = () => {
    return {
        drawerLabel: 'CHECKOUT',
        drawerIcon: () => <Feather name="check-circle" size={24}/>
    };
};

export default CheckOutScreen;