import React from "react";
import { Container, Content, Header, Body, Left, Right, Icon } from "native-base";
import { Text } from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";
import DrawerHeader from "../Components/DrawerHeader";

const UserScreen = ({navigation}) => {
    return (
        <Container>
            <DrawerHeader navigation={navigation} MenuName="User"/>
            <Content
                contentContainerStyle={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <Text>User Screen</Text>
            </Content>
        </Container>
    )
};

UserScreen.navigationOptions = () => {
    return {
        drawerLabel: 'USER',
        drawerIcon: () => <FontAwesome5 name="user" size={24}/>
    };
};

export default UserScreen;