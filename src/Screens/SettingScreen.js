import React from "react";
import { Container, Content, Header, Body, Left, Right, Icon } from "native-base";
import { Text } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import DrawerHeader from "../Components/DrawerHeader";

const SettingScreen = ({navigation}) => {
    return (
        <Container>
            <DrawerHeader navigation={navigation} MenuName="Setting"/>
            <Content
                contentContainerStyle={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <Text>Setting Screen</Text>
            </Content>
        </Container>
    )
};

SettingScreen.navigationOptions = () => {
    return {
        drawerLabel: 'SETTING',
        drawerIcon: () => <Ionicons name="md-settings" size={24}/>
    };
};

export default SettingScreen;