import React, {useState} from "react";
import { View, Text, Dimensions, StyleSheet } from "react-native";
import { Container, Content, Button, Spinner } from "native-base";
import { NavigationActions, StackActions } from 'react-navigation';
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

const WIDTH = Dimensions.get('window').width - 190;
const PrintReceiptScreen = ({navigation}) => {
    const [loading, setLoading] = useState(false);
    const resetHandler = (navigation) => {
        setLoading(true);
        setTimeout(function(){
            navigation.dispatch(StackActions.reset({
                index: 0,
                actions: [
                NavigationActions.navigate({routeName: 'Cart'})
            ]
            }));
            navigation.navigate('Checkout');
        }, 2000);
        
    };

    return (
        <Container style={{flex: 1}}>
            <Content>
                <View style={{flexDirection:'column', alignSelf:'center',marginTop: 150}}>
                    <View style={{width: WIDTH, marginHorizontal:80}}>
                        <FontAwesome5 name='receipt' size={80} style={{alignSelf:'center'}}/>
                        <Text style={styles.text_style_total}>TOTAL: </Text>
                        <Text style={{textAlign:'center', fontSize:34}}>RM 25.00 </Text>
                        <Text style={styles.text_style_change}>CHANGE: RM 5.00</Text>
                    </View>
                </View>
                {loading == true ? <Spinner /> : null}
            </Content>
            <View style={{marginBottom: 30, marginHorizontal:10}}>
                <View style={styles.button_footer}>
                    <Button block light>
                        <Text>PRINT RECEIPT</Text>
                    </Button>
                </View>
                <View style={styles.button_footer}>
                    <Button block light onPress={() => resetHandler(navigation)}>
                        <Text>START NEW SALE</Text>
                    </Button>
                </View>
            </View>
        </Container>
    )
};

const styles = StyleSheet.create({
    text_style_total:{
        fontSize: 22, 
        fontWeight:'bold', 
        textAlign:'center',
        marginTop: 20
    },
    text_style_change:{
        fontSize: 16, 
        fontWeight:'bold', 
        textAlign:'center'
    },
    button_footer:{
        marginTop:10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#bdc3c7', 
        overflow: 'hidden',
    },
    button_payment:{
        width: (WIDTH - 30 + 190) / 3, 
        height:80, 
        justifyContent:'center', 
        flexDirection:'column'
    }
});

export default PrintReceiptScreen;