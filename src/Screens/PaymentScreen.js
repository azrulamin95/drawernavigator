import React from "react";
import { View, Text, Dimensions, StyleSheet } from "react-native";
import { Container, Content, Button, Input, Item } from "native-base";
import StackNavigatorHeader from "../Components/StackNavigatorHeader";
import Entypo from "react-native-vector-icons/Entypo";
import FontAwesome from "react-native-vector-icons/FontAwesome";

const WIDTH = Dimensions.get('window').width - 190;

const PaymentScreen = ({navigation}) => {  
    return (
        <Container style={{flex: 1}}>
            <StackNavigatorHeader navigation={navigation} MenuName="Payment"/>
            <Content>
                <View style={{flexDirection:'column', alignSelf:'center',marginTop: 100}}>
                    <View style={{width: WIDTH, marginHorizontal:80}}>
                        <Text style={styles.text_style_receive}>RECEIVED: </Text>
                        <Item style={{alignSelf:'stretch'}}>
                            <Input placeholder="RM 30.00" style={{textAlign:'center', fontSize:30}} keyboardType='phone-pad' />
                        </Item>
                        <Text style={styles.text_style_change}>CHANGE: RM 5.00</Text>
                    </View>
                </View>                
            </Content>
            <View style={{marginBottom: 30, marginHorizontal:10}}>
                <View style={{flexDirection:'row', justifyContent:'space-between'}}>
                    <Button light style={styles.button_payment}>
                        <FontAwesome name='money' size={40}/>
                        <Text>CASH</Text>
                    </Button>
                    <Button light style={styles.button_payment}>
                        <FontAwesome name='credit-card' size={40}/>
                        <Text>CARD</Text>
                    </Button>
                    <Button light style={styles.button_payment}>
                        <Entypo name='dots-three-horizontal' size={40}/>
                        <Text>OTHERS</Text>
                    </Button>
                </View>
                <View style={styles.button_footer}>
                    <Button block light onPress={() => navigation.navigate('PrintReceipt')}>
                        <Text>CHARGE RM25.00</Text>
                    </Button>
                </View>
            </View>
        </Container>
    )
};

const styles = StyleSheet.create({
    text_style_receive:{
        fontSize: 22, 
        fontWeight:'bold', 
        textAlign:'center'
    },
    text_style_change:{
        fontSize: 18, 
        fontWeight:'bold', 
        textAlign:'center'
    },
    button_footer:{
        marginTop:10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#bdc3c7', 
        overflow: 'hidden',
    },
    button_payment:{
        width: (WIDTH - 30 + 190) / 3, 
        height:80, 
        justifyContent:'center', 
        flexDirection:'column'
    }
});

export default PaymentScreen;