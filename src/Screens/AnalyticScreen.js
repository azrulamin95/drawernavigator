import React from "react";
import { Container, Content, Header, Body, Left, Right, Icon } from "native-base";
import { Text } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import DrawerHeader from "../Components/DrawerHeader";

const AnalyticScreen = ({navigation}) => {
    return (
        <Container>
            <DrawerHeader navigation={navigation} MenuName="Analytics"/>
            <Content
                contentContainerStyle={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <Text>Analytic Screen</Text>
            </Content>
        </Container>
    )
};

AnalyticScreen.navigationOptions = () => {
    return {
        drawerLabel: 'ANALYTICS',
        drawerIcon: () => <Ionicons name="md-analytics" size={24}/>
    };
};

export default AnalyticScreen;