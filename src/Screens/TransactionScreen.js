import React  from "react";
import { Container, Content, Body, ListItem } from "native-base";
import { Text, StyleSheet, Image, Dimensions, FlatList, View } from "react-native";
import DrawerHeader from "../Components/DrawerHeader";
import ProductSearchTabs from "../Components/products/ProductSearchTabs";
import Entypo from "react-native-vector-icons/Entypo";
import FontAwesome from "react-native-vector-icons/FontAwesome";

const WIDTH = Dimensions.get('window').width;

const months = {
    '01' :  'JAN',
    '02' : 'FEB',
    '03' : 'MAR',
    '04' : 'APR',
    '05' : 'MAY',
    '06' :'JUN',
    '07' : 'JULY',
    '08' : 'AUG',
    '09' : 'SEPT',
    '10' : 'OCT',
    '11' : 'NOV',
    '12' : 'DEC'
};

export default class App extends React.Component {
    static navigationOptions = {
        drawerLabel: 'TRANSACTION',
        drawerIcon: () => <FontAwesome name="shopping-cart" size={24}/>
    };

    constructor() {
        super();
        this.state = {
            data : [
                {'prdt_name' : 'test1', 'date': '2020-01-01','total_trans': 5,'amount': '31.00','header': true}, 
                {'prdt_name' : 'test2','date': '2020-01-04','total_trans': 8,'amount': '121.00','header': true}, 
                {'prdt_name' : 'test3','date': '2020-01-05','total_trans': 1,'amount': '43.00','header': true}, 
                {'prdt_name' : 'test4','date': '2020-01-06','total_trans': 2,'amount': '23.00', 'header': true}, 
                {prdt_name: 'AC3231', 'price' : '12.00', 'qty': '1', 'date': '2020-01-04', 'time': '11:20', 'header': false}, 
                {prdt_name: 'AC3232', 'price' : '12.00', 'qty': '1', 'date': '2020-01-04', 'time': '11:20', 'header': false}, 
                {prdt_name: 'AC3233', 'price' : '12.00', 'qty': '1', 'date': '2020-01-04', 'time': '11:20', 'header': false}, 
                {prdt_name: 'AC3234', 'price' : '12.00', 'qty': '1', 'date': '2020-01-04', 'time': '11:20', 'header': false}, 
                {prdt_name: 'AC3235', 'price' : '12.00', 'qty': '1', 'date': '2020-01-04', 'time': '11:20', 'header': false}, 
                {prdt_name: 'XB01213', 'price' : '7.00', 'qty': '1', 'date': '2020-01-01', 'time': '13:20', 'header': false},
                {prdt_name: 'XB01214', 'price' : '6.00', 'qty': '1', 'date': '2020-01-01', 'time': '14:10', 'header': false},
                {prdt_name: 'XB01215', 'price' : '6.00', 'qty': '1', 'date': '2020-01-01', 'time': '14:25', 'header': false},
                {prdt_name: 'XB01216', 'price' : '6.00', 'qty': '1', 'date': '2020-01-01', 'time': '15:04', 'header': false},
                {prdt_name: 'XB01217', 'price' : '6.00', 'qty': '1', 'date': '2020-01-01', 'time': '15:44', 'header': false},
                {prdt_name: 'AC3236', 'price' : '12.00', 'qty': '1', 'date': '2020-01-04', 'time': '11.20', 'header': false},
                {prdt_name: 'AC3237', 'price' : '12.00', 'qty': '1', 'date': '2020-01-04', 'time': '11.20', 'header': false},
                {prdt_name: 'AC3238', 'price' : '12.00', 'qty': '1', 'date': '2020-01-04', 'time': '11.20', 'header': false},
                {prdt_name: 'Book26', 'price' : '12.00', 'qty': '1', 'date': '2020-01-05', 'time': '11.20', 'header': false},
                {prdt_name: 'Book27', 'price' : '12.00', 'qty': '1', 'date': '2020-01-06', 'time': '11.20', 'header': false},
                {prdt_name: 'Book28', 'price' : '12.00', 'qty': '1', 'date': '2020-01-06', 'time': '11.20', 'header': false},
            ].sort((a, b) => a.date < b.date ? -1 : 1),
            stickyHeaderIndices: []
        };
    }

    componentWillMount() {
        var arr = [];
        this.state.data.map(obj => {
        if (obj.header) {
            arr.push(this.state.data.indexOf(obj));
        }
        });
        arr.push(0);
        this.setState({
        stickyHeaderIndices: arr
        });
    }

    renderItem = ({ item }) => {
        if (item.header) {
            var parts =item.date.split('-');
            return (
                <ListItem itemDivider>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{ fontWeight: "bold", fontSize: 16 }}>
                            {parseInt(parts[2])} {months[parts[1]]} 
                        </Text>
                        <Text style={{ fontSize: 16, marginLeft: 5 }}>RM {item.amount}</Text>
                        <Entypo name="dot-single" size={22}/>
                        <Text style={{ fontSize: 16 }}>{item.total_trans}</Text>
                    </View>
                </ListItem>
            );
        } else if (!item.header) {
            return (
                <ListItem>
                    <View style={{flexDirection: 'row',}}>
                        <View>
                            <FontAwesome name="money" size={32}/>
                        </View>
                        <View style={{flexDirection: 'column', marginLeft: 20, flex: 1}}>
                            <View style={{flexDirection:'row', justifyContent: 'space-between', flex:1}}>
                                <Text style={{ fontWeight: "bold", fontSize: 16 }}>RM {item.price}</Text>
                                <Text style={{ fontSize: 16 }}>{item.time}</Text>
                            </View>
                            <Text>{item.prdt_name}</Text>
                        </View>
                    </View>
                </ListItem>
            );
        }
    };
    render() {
        const { navigation } = this.props;
        return (
            <Container>
                <DrawerHeader navigation={navigation} MenuName="Transaction"/>
                {this.state.data.length > 0 ? <ProductSearchTabs navigation={navigation}/> : null}
                {this.state.data.length > 0 ? 
                    <FlatList
                        data={this.state.data}
                        renderItem={this.renderItem}
                        keyExtractor={item => item.prdt_name}
                        stickyHeaderIndices={this.state.stickyHeaderIndices}
                    />
                : <Content contentContainerStyle={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                    <Image source={require('../../assets/no_record.png')} style={{width: 210, height: 210}}/>
                    <Text style={styles.no_sale_text}>You haven't made a sale during this period.</Text>
                </Content>}
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    empty_container : {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor:'yellow'
    },
    no_sale_text:{
        fontSize:26, 
        width: WIDTH - 80,
        fontFamily: 'sans-serif-condensed',
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 10
    },
    container: {
        flex: 1,
        marginHorizontal: 16,
    },
        item: {
        backgroundColor: '#f9c2ff',
        padding: 20,
        marginVertical: 8,
    },
        header: {
        fontSize: 32,
    },
        title: {
        fontSize: 24,
    },
});