import React from "react";
import { Container, Content, Header, Body, Left, Right, Icon } from "native-base";
import { Text } from "react-native";
import FontAwesome5 from "react-native-vector-icons/FontAwesome5";

import DrawerHeader from "../Components/DrawerHeader";
const CustomerScreen = ({navigation}) => {
    return (
        <Container>
            <DrawerHeader navigation={navigation} MenuName="Customers"/>
            <Content
                contentContainerStyle={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <Text>Customer Screen</Text>
            </Content>
        </Container>
    )
};

CustomerScreen.navigationOptions = () => {
    return {
        drawerLabel: 'CUSTOMERS',
        drawerIcon: () => <FontAwesome5 name="user-tie" size={24}/>
    };
};

export default CustomerScreen;