import React from "react";
import { Container, Content, Header, Body, Left, Right, Icon } from "native-base";
import { Text } from "react-native";
import Ionicons from "react-native-vector-icons/Ionicons";
import DrawerHeader from "../Components/DrawerHeader";

const HelpScreen = ({navigation}) => {
    return (
        <Container>
            <DrawerHeader navigation={navigation} MenuName="Help"/>
            <Content
                contentContainerStyle={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <Text>Help Screen</Text>
            </Content>
        </Container>
    )
};

HelpScreen.navigationOptions = () => {
    return {
        drawerLabel: 'HELP',
        drawerIcon: () => <Ionicons name="md-help-circle-outline" size={24}/>
    };
};

export default HelpScreen;