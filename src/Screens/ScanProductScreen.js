import React, {useState} from "react";
import * as Permissions from 'expo-permissions';
import { BarCodeScanner } from 'expo-barcode-scanner';
import { Camera } from 'expo-camera';
import { Container, Content, List, ListItem, ActionSheet, Button } from "native-base";
import { View, Text, StyleSheet, Dimensions } from "react-native";
import { withNavigationFocus } from "react-navigation";
import StackNavigatorHeader from "../Components/StackNavigatorHeader";
import Entypo from "react-native-vector-icons/Entypo";

var BUTTONS = [
    { text: "ADD NOTE", icon: "paper", iconColor: "#2c8ef4" },
    { text: "ADD DISCOUNT", icon: "cash", iconColor: "green" },
    { text: "CLEAR CART", icon: "trash", iconColor: "#fa213b" },
    { text: "CANCEL", icon: "close", iconColor: "#25de5b" }
  ];
var DESTRUCTIVE_INDEX = 2;
var CANCEL_INDEX = 3;

class BarcodeScanner extends React.Component {
    constructor(props) {
        super(props);
    }
    
    state = {
        hasCameraPermission: null,
        scanned: false,
        state: {},
    };
    
    async componentDidMount() {
        this.getPermissionsAsync();
    }
    
    getPermissionsAsync = async () => {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === 'granted' });
    };

    render() {
        const { hasCameraPermission, scanned } = this.state;
        const { navigation } = this.props;
        const isFocused = this.props.navigation.isFocused();

        if(!isFocused){
            return null;
        }
        else if (hasCameraPermission === null) {
            return <Text>Requesting for camera permission</Text>;
        }
        if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>;
        }
        return (
            <Container>
                <StackNavigatorHeader navigation={navigation} MenuName="Scan Product"/>
                    <Content>
                        <View style={{ flex: 1 }}>
                            <View>
                                <Camera
                                    onBarCodeScanned={scanned ? undefined : this.handleBarCodeScanned}
                                    style={{height: 200}}
                                />
                                {scanned && (
                                    <Button title={'Tap to Scan Again'} onPress={() => this.setState({ scanned: false })} />
                                )}
                            </View>
                            <View>
                                <List>
                                    <ListItem itemDivider style={styles.head_list}>
                                        <View style={styles.item_list}>
                                            <Text>1x MATH</Text>
                                            <Text>1x RM 15.00</Text>
                                        </View>
                                    </ListItem>
                                    <ListItem itemDivider style={styles.head_list}>
                                        <View style={styles.item_list}>
                                            <Text>1x ENGLISH</Text>
                                            <Text>1x RM 12.00</Text>
                                        </View>
                                    </ListItem>
                                </List>
                            </View>
                            <View style={styles.summary_style}>
                                <Button small light style={styles.button_style}>
                                    <Text> ADD DISCOUNT </Text>
                                </Button>
                            </View>
                            <View style={styles.summary_style}>
                                <Text style={{fontWeight:'bold', fontSize: 14}}>TOTAL : RM 10.00</Text>
                            </View>
                        </View>
                    </Content>
                    <View style={{flexDirection:'row', marginLeft:10}}>
                        <View style={styles.icon_button_container}>
                            <Button light style={styles.button_style} onPress={() => 
                                ActionSheet.show(
                                    {
                                        options: BUTTONS,
                                        cancelButtonIndex: CANCEL_INDEX,
                                        destructiveButtonIndex: DESTRUCTIVE_INDEX,
                                        title: "MORE OPTION"
                                    },
                                    buttonIndex => {
                                        this.state = { clicked: BUTTONS[buttonIndex] };
                                    }
                                )
                            }>
                                <Entypo name='dots-three-vertical' />
                            </Button>
                        </View>
                        <View style={styles.footer_container}>
                            <Button block light onPress={() => navigation.navigate('Payment')}>
                                <Text>2 item = RM39.00</Text>
                            </Button>
                        </View>
                    </View>
            </Container>
        );
    }
    
    handleBarCodeScanned = ({ type, data }) => {
        this.setState({ scanned: true });
        alert(`Bar code with type ${type} and data ${data} has been scanned!`);
    };
}

const styles = StyleSheet.create({
    cameraContainer: {
        height: "100%",
    },
    head_list:{
        height: 50,
        marginTop:5
    },
    item_list:{
        flexDirection:'row',
        justifyContent:'space-between',
        flex: 1
    },
    summary_style:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end',
        marginTop:10, 
        marginRight: 10
    },
    footer_container:{
        marginHorizontal: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#bdc3c7', 
        overflow: 'hidden',
        marginBottom: 30,
        flex:1
    },
    icon_button_container:{
        width:30,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#bdc3c7', 
        overflow: 'hidden',
        marginBottom: 30,
    },
    button_style:{
        alignItems:'center',
        justifyContent:'center'
    }
});

export default withNavigationFocus(BarcodeScanner);