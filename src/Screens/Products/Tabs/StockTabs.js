import React from "react";
import { Container, Content } from "native-base";
import { FlatList, StyleSheet, View } from "react-native";

import ProductSearchTabs from "../../../Components/products/ProductSearchTabs";
import ProductList from "../../../Components/products/ProductList";

const StockTabs = ({navigation}) => {
    const product = [
        {title: 'Book', 'price' : '400', 'img': require('../../../../assets/no_image.png')}, 
        {title: 'Pen', 'price' : '600', 'img': require('../../../../assets/no_image.png')},
        {title: 'Pencil', 'price' : '200', 'img': require('../../../../assets/no_image.png')},
    ];
    return (
        <Container>
            <ProductSearchTabs navigation={navigation}/>
            <Content>
                <View style={styles.list_container}>
                    <FlatList 
                        data={product} 
                        renderItem={
                            ({item}) => <ProductList 
                                navigation={navigation}
                                title={item.title}
                                price={item.price}
                                img={item.img}
                                list='stock'
                            />
                        } 
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </Content>
        </Container>
    )
};

const styles = StyleSheet.create({
    head_container:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop: 15,
        marginHorizontal : 10,
        height: 30,
    }
});

export default StockTabs;