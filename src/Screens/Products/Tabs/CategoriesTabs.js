import React from "react";
import { Container, Content, Input } from "native-base";
import { Text, StyleSheet, View } from "react-native";

import ProductSearchTabs from "../../../Components/products/ProductSearchTabs";

const CategoriesTabs = ({navigation}) => {
    return (
        <Container>
            <ProductSearchTabs navigation={navigation}/>
            <Content>
            </Content>
        </Container>
    )
};

const styles = StyleSheet.create({
    head_container:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop: 15,
        marginHorizontal : 10,
        height: 30,
    }
});

export default CategoriesTabs;