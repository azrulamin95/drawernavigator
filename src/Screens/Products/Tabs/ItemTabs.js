import React from "react";
import { Container, Content } from "native-base";
import { FlatList, Text, StyleSheet, View } from "react-native";

import ProductSearchTabs from "../../../Components/products/ProductSearchTabs";
import ProductList from "../../../Components/products/ProductList";

const ItemTabs = ({navigation}) => {
    const product = [
        {title: 'Book', 'price' : '12.00', 'img': require('../../../../assets/no_image.png')}, 
        {title: 'Pen', 'price' : '6.00', 'img': require('../../../../assets/no_image.png')},
        {title: 'Pencil', 'price' : '2.00', 'img': require('../../../../assets/no_image.png')},
        {title: 'Notebook', 'price' : '2.00', 'img': require('../../../../assets/no_image.png')},
        {title: 'Perfume', 'price' : '2.00', 'img': require('../../../../assets/no_image.png')},
        {title: 'Marker', 'price' : '2.00', 'img': require('../../../../assets/no_image.png')},
        {title: 'Highlighter', 'price' : '2.00', 'img': require('../../../../assets/no_image.png')},
        {title: 'Stapler', 'price' : '2.00', 'img': require('../../../../assets/no_image.png')},
        {title: 'Stapler', 'price' : '2.00', 'img': require('../../../../assets/no_image.png')},
        {title: 'Stapler', 'price' : '2.00', 'img': require('../../../../assets/no_image.png')},
    ];
    return (
        <Container>
            <ProductSearchTabs navigation={navigation}/>
            <Content>
                <View style={styles.list_container}>
                    <FlatList 
                        data={product} 
                        renderItem={
                            ({item}) => <ProductList 
                                navigation={navigation}
                                title={item.title}
                                price={item.price}
                                img={item.img}
                                list='product'
                            />
                        } 
                        keyExtractor={(item, index) => index.toString()}
                    />
                </View>
            </Content>
        </Container>
    )
};

const styles = StyleSheet.create({
    head_container:{
        flexDirection:'row',
        justifyContent:'space-between',
        marginTop: 15,
        marginHorizontal : 10,
        height: 30,
    }, 
    list_container:{
        marginHorizontal: 10
    }
});

export default ItemTabs;