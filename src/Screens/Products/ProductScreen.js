import React from "react";
import { Container, Content, Header, Body, Left, Right, Icon, Tab, Tabs } from "native-base";
import { Text } from "react-native";
import FontAwesome from "react-native-vector-icons/FontAwesome";

/*tabs*/
import ItemTabs from "./Tabs/ItemTabs";
import StockTabs from "./Tabs/StockTabs";
import CategoriesTabs from "./Tabs/CategoriesTabs";

const ProductScreen = ({navigation}) => {
    return (
        <Container>
            <Header hasTabs>
                <Left style={{paddingLeft:10}}>
                    <Icon
                        name="ios-menu"
                        onPress={() => navigation.openDrawer('DrawerOpen')} 
                        style={{color: 'white'}}
                    />
                </Left>
                <Body>
                    <Text style={{fontSize: 18, fontWeight: 'bold', color: 'white'}}>Products</Text>
                </Body>
                <Right />
            </Header>
            <Content
                contentContainerStyle={{
                    flex: 1,
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <Tabs>
                    <Tab heading="ITEMS">
                        <ItemTabs navigation={navigation}/>
                    </Tab>
                    <Tab heading="STOCK">
                        <StockTabs />
                    </Tab>
                    <Tab heading="CATEGORIES">
                        <CategoriesTabs />
                    </Tab>
                </Tabs>
            </Content>
        </Container>
    )
};

ProductScreen.navigationOptions = () => {
    return {
        drawerLabel: 'PRODUCTS',
        drawerIcon: () => <FontAwesome name="shopping-bag" size={24}/>
    };
};

export default ProductScreen;