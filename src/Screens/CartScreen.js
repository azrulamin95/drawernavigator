import React, {useState} from "react";
import { View, Text, StyleSheet } from "react-native";
import { Container, Content, Button, List, ListItem, ActionSheet } from "native-base";
import StackNavigatorHeader from "../Components/StackNavigatorHeader";
import CartListItem from "../Components/CartListItem";
import Entypo from "react-native-vector-icons/Entypo";

//var BUTTONS = ["ADD NOTE", "ADD DISCOUNT", "CLEAR CART", "Delete", "Cancel"];

var BUTTONS = [
   { text: "ADD NOTE", icon: "paper", iconColor: "#2c8ef4" },
   { text: "ADD DISCOUNT", icon: "cash", iconColor: "green" },
   { text: "CLEAR CART", icon: "trash", iconColor: "#fa213b" },
   { text: "CANCEL", icon: "close", iconColor: "#25de5b" }
 ];
var DESTRUCTIVE_INDEX = 2;
var CANCEL_INDEX = 3;

const CartScreen = ({navigation}) => {
    const [state, setState] = useState({});
    return (
        <Container style={{flex: 1}}>
            <StackNavigatorHeader navigation={navigation} MenuName="Cart"/>
            <Content padder>
                <List>
                    <ListItem itemDivider style={styles.head_list}>
                        <Text>MATH X 1</Text>
                    </ListItem>                    
                    <ListItem style={styles.body_list}>
                        <CartListItem icon="inbox" detail="1"/>
                        <CartListItem icon="money" detail="15.00"/>
                        <CartListItem icon="scissors" detail="5.00"/>
                    </ListItem>

                    <ListItem itemDivider style={styles.head_list}>
                        <Text>ENGLISH X 2</Text>
                    </ListItem>
                </List>
                <View style={styles.summary_style}>
                    <Button small light style={styles.button_style}>
                        <Text> ADD DISCOUNT </Text>
                    </Button>
                </View>
                <View style={styles.summary_style}>
                    <Text style={{fontWeight:'bold', fontSize: 14}}>TOTAL : RM 10.00</Text>
                </View>
            </Content>
            <View style={{flexDirection:'row', marginLeft:10}}>
                <View style={styles.icon_button_container}>
                    <Button light style={styles.button_style} onPress={() => 
                        ActionSheet.show(
                            {
                              options: BUTTONS,
                              cancelButtonIndex: CANCEL_INDEX,
                              destructiveButtonIndex: DESTRUCTIVE_INDEX,
                              title: "MORE OPTION"
                            },
                            buttonIndex => {
                              setState({ clicked: BUTTONS[buttonIndex] });
                            }
                          )
                    }>
                        <Entypo name='dots-three-vertical' />
                    </Button>
                </View>
                <View style={styles.footer_container}>
                    <Button block light onPress={() => navigation.navigate('Payment')}>
                        <Text>2 item = RM39.00</Text>
                    </Button>
                </View>
            </View>
        </Container>
    )
};

const styles = StyleSheet.create({
    head_list:{
        height: 50,
        marginTop:5
    },
    body_list:{
        flexDirection:'row',
        height: 80, 
        flex: 1,
        justifyContent: 'space-between', 
        marginLeft:0,
        paddingLeft: 0
    },
    footer_container:{
        marginHorizontal: 10,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#bdc3c7', 
        overflow: 'hidden',
        marginBottom: 30,
        flex:1
    },
    icon_button_container:{
        width:30,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#bdc3c7', 
        overflow: 'hidden',
        marginBottom: 30,
    },
    button_style:{
        alignItems:'center',
        justifyContent:'center'
    },
    summary_style:{
        flex:1,
        flexDirection:'row',
        justifyContent:'flex-end',
        marginTop:10, 
        marginRight: 10
    },
    marginRight:{
        marginRight: 10
    }
});

export default CartScreen;