import React, { Component } from 'react';
import { 
    View, 
    Text, 
    StyleSheet, 
    Dimensions, 
    TextInput, 
    TouchableOpacity, 
    ActivityIndicator, 
    KeyboardAvoidingView 
} from 'react-native';
import Svg, { Image, Circle, ClipPath } from "react-native-svg";
import Animated, { Easing } from 'react-native-reanimated';
import { TapGestureHandler, State, FlatList } from 'react-native-gesture-handler';
import { NavigationActions, StackActions } from 'react-navigation';
const { width, height } = Dimensions.get('window');

const {
  Value,
  event,
  block,
  cond,
  eq,
  set,
  Clock,
  startClock,
  stopClock,
  debug,
  timing,
  clockRunning,
  interpolate,
  Extrapolate,
  concat
} = Animated;

function runTiming(clock, value, dest) {
  const state = {
    finished: new Value(0),
    position: new Value(0),
    time: new Value(0),
    frameTime: new Value(0),
    isLoading: false
  };

  const config = {
    duration: 1000,
    toValue: new Value(0),
    easing: Easing.inOut(Easing.ease)
  };

  return block([
    cond(clockRunning(clock), 0, [
      set(state.finished, 0),
      set(state.time, 0),
      set(state.position, value),
      set(state.frameTime, 0),
      set(config.toValue, dest),
      startClock(clock)
    ]),
    timing(clock, state, config),
    cond(state.finished, debug('stop clock', stopClock(clock))),
    state.position
  ]);
}

class SignInScreen extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false
    };

    changeState = () => {
      this.setState({isLoading: true});
      const that = this;
      setTimeout(function(){
        that.setState({isLoading: false});
        that.onCloseState = event([
        {
            nativeEvent: ({ state }) =>
            block([
                cond(
                eq(state, State.END),
                set(that.buttonOpacity, runTiming(new Clock(), 0, 1))
                )
            ])
        }
        ]);
        props.navigation.navigate('Checkout');
        // const resetAction = StackActions.reset({
            //     index: 0,
            //     key: undefined,
            //     actions: [NavigationActions.navigate({ routeName: 'PrintReceipt' })],
            // });
            // navigation.dispatch(resetAction);
      }, 1000);
    };

    this.buttonOpacity = new Value(1);

    this.onStateChange = event([
      {
        nativeEvent: ({ state }) =>
          block([
            cond(
              eq(state, State.END),
              set(this.buttonOpacity, runTiming(new Clock(), 1, 0))
            )
          ])
      }
    ]);

    this.onCloseState = event([
      {
        nativeEvent: ({ state }) =>
          block([
            cond(
              eq(state, State.END),
              set(this.buttonOpacity, runTiming(new Clock(), 0, 1))
            )
          ])
      }
    ]);
    
    this.buttonY = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [100, 0],
      extrapolate: Extrapolate.CLAMP
    });

    this.bgY = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [-height / 3 - 50, 0],
      extrapolate: Extrapolate.CLAMP
    });

    this.textInputZindex = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [1,-1],
      extrapolate: Extrapolate.CLAMP
    });

    this.textInputY = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [0,100],
      extrapolate: Extrapolate.CLAMP
    });

    this.textInputOpacity = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [1, 0],
      extrapolate: Extrapolate.CLAMP
    });

    this.rotateCross = interpolate(this.buttonOpacity, {
      inputRange: [0, 1],
      outputRange: [180, 360],
      extrapolate: Extrapolate.CLAMP
    });
  }

  render() {
     
    // changeState = (value) => {
    //   this.setState(this.isLoading = value);
    //   //navigation.navigate('drawerFlow');
    // };

    return (
      <KeyboardAvoidingView style={{ flex: 1, backgroundColor: 'white', justifyContent: 'flex-end', }} behavior="padding" enabled>

        <Animated.View
          style={{
            ...StyleSheet.absoluteFill,
            transform: [{ translateY: this.bgY }]
          }}
        >

          <Svg height={height + 50} width={width}>
            <ClipPath id="clip">
              <Circle r={height + 50} cx={width /2} />
            </ClipPath>
            <Image
              href={require('../../assets/pos_background.jpg')}
              width={width}
              height={height+ 50}
              preserveAspectRatio='xMidYmid slice'
              clipPath="url(#clip)"
            />
          </Svg>
        </Animated.View>
        <View style={{ height: height / 3, justifyContent: 'center' }}>
          <TapGestureHandler onHandlerStateChange={this.onStateChange}>
            <Animated.View
              style={{
                ...styles.button,
                opacity: this.buttonOpacity,
                transform: [{ translateY: this.buttonY }]
              }}
            >
              <Text style={{ fontSize: 16, fontWeight: 'bold' }}>SIGN IN</Text>
            </Animated.View>
          </TapGestureHandler>
          <Animated.View
            style={{
              ...styles.button,
              backgroundColor: '#2E71DC',
              opacity: this.buttonOpacity,
              transform: [{ translateY: this.buttonY }]
            }}
          >
            <Text style={{ fontSize: 16, fontWeight: 'bold', color: 'white' }}>
              SIGN IN WITH FACEBOOK
            </Text>
          </Animated.View>

          <Animated.View
            style={{
              zIndex: this.textInputZindex,
              opacity: this.textInputOpacity,
              transform: [{translateY: this.textInputY}],
              height: height/3,
              ...StyleSheet.absoluteFill,
              top: null,
              justifyContent: 'center'
            }}
          >

            <TapGestureHandler onHandlerStateChange={this.onCloseState}>
              <Animated.View style={styles.closeButton}>
                <Animated.Text style={{fontSize:15, transform:[{
                  rotate: concat(this.rotateCross, 'deg')
                }]}}>X</Animated.Text>
              </Animated.View>
            </TapGestureHandler>

            <TextInput 
                placeholder="EMAIL"
                style={styles.textInput}
                placeholderTextColor="black"
            />
            <TextInput 
                placeholder="PASSWORD"
                style={styles.textInput}
                placeholderTextColor="black"
            />

            {this.state.isLoading ? <ActivityIndicator size="large" color="green" animating={true}/> : null}

            <TouchableOpacity onPress={() => changeState()}>
              <Animated.View style={styles.button_signIn}>
                <Text style={{fontSize: 16, fontWeight: 'bold'}}>SIGN IN</Text>
              </Animated.View>
            </TouchableOpacity>

          </Animated.View>
        </View>
      </KeyboardAvoidingView>
    );
  }
}

SignInScreen.navigationOptions = () => {
  return {
      header: null
  };
};

export default SignInScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  },
  spinner:{

  },
  button: {
    backgroundColor: 'white',
    height: 50,
    marginHorizontal: 20,
    borderRadius: 35,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
    shadowOffset: {width:2, height: 2},
    shadowColor: 'black',
    shadowOpacity: 0.2
  },
  button_signIn: {
    backgroundColor: 'white',
    height: 50,
    marginHorizontal: 20,
    borderRadius: 35,
    alignItems: 'center',
    justifyContent: 'center',
    marginVertical: 5,
    shadowOffset: {width:2, height: 2},
    shadowColor: 'black',
    shadowOpacity: 0.2,
    borderColor: 'black',
    borderStyle: 'solid',
    borderWidth: 1
  },
  textInput:{
    height: 50,
    borderRadius: 25,
    borderWidth: 0.5,
    marginHorizontal: 20,
    paddingLeft: 10,
    marginVertical: 5,
    borderColor: 'rgba(0,0,0,0.2)',
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
  },
  closeButton:{
    height: 40,
    width: 40,
    backgroundColor: 'white',
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    top: -20,
    left: width / 2 - 20,
    shadowOffset: {width:2, height: 2},
    shadowColor: 'black',
    shadowOpacity: 0.2
  }
});