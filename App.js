import React from 'react';
import { StyleSheet, Text, View, Image, StatusBar } from 'react-native';
import { Container, Content, Header, Body, Root } from "native-base";
import { createDrawerNavigator, DrawerItems } from 'react-navigation-drawer';
import { createStackNavigator } from 'react-navigation-stack';
import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import { Transition } from 'react-native-reanimated';
import 
{ 
  createAppContainer, 
  createSwitchNavigator
} from 'react-navigation';

/*import page*/
import SIgnInScreen from "./src/Screens/SIgnInScreen";
import CheckOutScreen from "./src/Screens/CheckOutScreen";
import ProductScreen from "./src/Screens/Products/ProductScreen";
import CustomerScreen from "./src/Screens/CustomerScreen";
import TransactionScreen from "./src/Screens/TransactionScreen";
import AnalyticScreen from "./src/Screens/AnalyticScreen";
import UserScreen from "./src/Screens/UserScreen";
import SettingScreen from "./src/Screens/SettingScreen";
import HelpScreen from "./src/Screens/HelpScreen";
import CartScreen from "./src/Screens/CartScreen";
import PaymentScreen from "./src/Screens/PaymentScreen";
import PrintReceiptScreen from "./src/Screens/PrintReceiptScreen";
import SignInScreen from './src/Screens/SIgnInScreen';
import ScanProductScreen from './src/Screens/ScanProductScreen';
import AddEditProduct from "./src/Screens/Products/AddEditProduct";

const customDrawer = (props) => {
  return(
    <Container>
      <Header style={{height: 150}}>
        <Body style={{flex: 1, flexDirection: 'row', alignItems: 'center'}}>
          <Image style={{height: 110, width: 110}} source={require("./assets/profile_1.png")}/>
          <View style={{paddingLeft: 20}}>
            <Text style={{fontSize: 22, fontWeight: 'bold', fontFamily:'sans-serif', color: 'white'}}>Azrul Amin</Text>
            <Text style={{fontSize: 12, fontWeight: 'bold', fontFamily:'sans-serif', color: 'white'}}>* Cashier</Text>
          </View>
        </Body>
      </Header>
      <Content>
        <DrawerItems {...props}/>
      </Content>
    </Container>
  )
};

const MyNavigator = createDrawerNavigator({
    CheckoutFlow: createStackNavigator({
      Cart: CartScreen,
      Payment: PaymentScreen,
      PrintReceipt : PrintReceiptScreen,
    }, {
        headerMode: 'none',
        navigationOptions: {
          headershown: false,
          header: null,
          drawerLockMode: 'locked-closed',
          drawerLabel: () => null
        },
    }),
    ScanProductFlow: createStackNavigator({
      ScanProductScreen: ScanProductScreen,
    }, {
        headerMode: 'none',
        navigationOptions: {
          headershown: false,
          header: null,
          drawerLockMode: 'locked-closed',
          drawerLabel: () => null
        },
    }),
    AddEditProductFlow: createStackNavigator({
      AddEditProduct: AddEditProduct,
    }, {
        headerMode: 'none',
        navigationOptions: {
          headershown: false,
          header: null,
          drawerLockMode: 'locked-closed',
          drawerLabel: () => null
        },
    }),
    Checkout : CheckOutScreen,
    Product : ProductScreen,
    Customer: CustomerScreen,
    Transaction: TransactionScreen,
    Analytic: AnalyticScreen,
    User: UserScreen,
    Setting: SettingScreen,
    Help: HelpScreen,
  },{
    initialRouteName: 'Checkout',
    contentComponent: customDrawer,
    drawerOpenRoute: 'DrawerOpen',
    draweCloseRoute: 'DrawerClose',
    drawerToggleRoute: 'DrawerToggle',
    animationEnabled:true,
});

const MySwitchNavigator = createSwitchNavigator(
  {
    AuthFlow: createStackNavigator({
      SignIn: SignInScreen,
    }, {
        headerMode: 'none',
        navigationOptions: {
          headershown: false,
          header: null,
        },
    }),
    MyNavigator: MyNavigator,
  }
);

const App = createAppContainer(MySwitchNavigator);
StatusBar.setHidden(true);

export default () => {
  return (
    <Root>
      <App />
    </Root>
  );
};